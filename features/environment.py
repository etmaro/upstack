import features.steps.json_responses as json_responses


def before_all(context):
    context.base_url = "http://127.0.0.1:8000/api"
    context.headers = {'Content-Type': 'application/json'}
    context.json_responses = json_responses

