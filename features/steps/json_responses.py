import trafaret as t

newGameData = t.Dict({
    t.Key('id'): t.Int,
    t.Key('guesses'): t.Int,
    t.Key('progress'): t.String,
    t.Key('instructions'): t.String,
})
