from behave_rest.steps import *
import json
import requests


TYPES = {
    "str": str,
    "int": int,
    "bool": bool,
}


def cast(type_var, value, context):
    if value.startswith('context.'):
        return eval(value)
    return TYPES[type_var](value)


@step('I make a {request_verb} request to "{url_path_segment}" with json parameters')
def request_with_parameters(context, request_verb, url_path_segment):
    if not hasattr(context, 'verify_ssl'):
        context.verify_ssl = True

    url = context.base_url + '/' + url_path_segment

    params = {}

    for row in context.table:
        for x in context.table.headings:
            params[x] = row[x]

    context.r = getattr(requests, request_verb.lower())(url, json.dumps(params), headers=context.headers, verify=context.verify_ssl)

    log_full(context.r)
    return context.r


@step('the {var_type} "{name}" in the response equals "{value}"')
def check_var(context, var_type, name, value):
        casted_val = cast(var_type, value, context)
        assert casted_val == context.r.json()[name]


@step('the {var_type} "{name}" in the response is one of "{values}"')
def check_var_multiple_values(context, var_type, name, values):
    values_list = values.split(', ')
    casted_values = []
    for value in values_list:
        casted_values.append(cast(var_type, value, context))
    correct_response = False
    for casted_value in casted_values:
        if casted_value == context.r.json()[name]:
            correct_response = True
            break
    assert correct_response
