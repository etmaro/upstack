Feature: Testing battleship game API

    Scenario: create game
        When I make a POST request to "game/"
        Then the response status code should equal 200
        And the response structure should equal "newGameData"
    
    Scenario: hit water
        When I make a POST request to "game/1/shoot/" with json parameters
            |x|y|
            |1|3|
        Then the response status code should equal 200

    Scenario: hit water
        When I make a POST request to "game/1/shoot/" with json parameters
            |x|y|
            |1|0|
        Then the response status code should equal 200
        And the str "result" in the response is one of "Hit ship, Sank Cruiser, Sank Destroyer, Sank Submarine, Sank Battleship, Sank Carrier"

    Scenario: out of bounds 
        When I make a POST request to "game/1/shoot/" with json parameters
            |x|y|
            |13|3|
        Then the response status code should equal 200
        And the str "result" in the response equals "Out of bounds"
