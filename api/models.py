import random
from django.db import models


SHIPS = {
    'CA': {'name': 'Carrier', 'length': 5},
    'BA': {'name': 'Battleship', 'length': 4},
    'CR': {'name': 'Cruiser', 'length': 3},
    'DE': {'name': 'Destroyer', 'length': 2},
    'SU': {'name': 'Submarine', 'length': 1},
}


class Game(models.Model):
    guesses = models.IntegerField('Guesses', default=0)

    @property
    def ships_remaining(self):
        """
        Return ships where at least one coordinate isn't hit.
        If all ships are hit -ergo game over- returns an empty set.
        """
        ships_remaining = Coordinate.objects.filter(ship__game_id=self.id, hit=False)
        ships_remaining_names = {c.ship.get_name_display() for c in ships_remaining}
        return ships_remaining_names and ', '.join(ships_remaining_names) or ''

    def add_ships(self):
        """
        Add ships for game in random coordinates.
        Checks ship coordinates does not collide or touch other ships.
        """
        coords_taken = set()
        for ship_id in SHIPS:
            valid_placement = False
            length = SHIPS[ship_id]['length']
            name = SHIPS[ship_id]['name']
            while not valid_placement:
                # vertical or horizontal direction
                vertical = random.randint(0, 1)
                if vertical:
                    # randomize starting point for ship placement
                    x_start_coord = random.randint(0, 9)
                    # substract length so it stays on board
                    y_start_coord = random.randint(0, 9-length)
                    # pretended coords for the ship (list of tuples)
                    coords_pretended = [(x_start_coord, y) for y in range(y_start_coord, y_start_coord+length)]
                    x_delta = (x_start_coord-1 >= 0 and x_start_coord-1 or 0,
                               x_start_coord+2 <= 10 and x_start_coord+2 or 10)
                    y_delta = (y_start_coord-1 >= 0 and y_start_coord-1 or 0,
                               y_start_coord+length+1 <= 10 and y_start_coord+length+1 or 10)
                    # area sorrounding the ship (set of tuples)
                    coords_surrounded = {(x, y) for y in range(*y_delta) for x in range(*x_delta)}
                else:
                    # analogous to previous section but different direction
                    x_start_coord = random.randint(0, 9-length)
                    y_start_coord = random.randint(0, 9)
                    coords_pretended = [(x, y_start_coord) for x in range(x_start_coord, x_start_coord+length)]
                    x_delta = (x_start_coord-1 >= 0 and x_start_coord-1 or 0,
                               x_start_coord+length+1 <= 10 and x_start_coord+length+1 or 10)
                    y_delta = (y_start_coord-1 >= 0 and y_start_coord-1 or 0,
                               y_start_coord+2 <= 10 and y_start_coord+2 or 10)
                    coords_surrounded = {(x, y) for y in range(*y_delta) for x in range(*x_delta)}
                # checks there is no collision between ships (including surrounding areas)
                if not (coords_taken & coords_surrounded):
                    ship = Ship.objects.create(name=name, length=length, game=self)
                    for coord in coords_pretended:
                        Coordinate.objects.create(x_coord=coord[0], y_coord=coord[1], ship=ship)
                    coords_taken.update(coords_pretended)
                    valid_placement = True

    def shoot(self, x, y):
        """
        Returns the ship that one of its coordinates math given x & y.
        None if no ship match.
        """
        coord = Coordinate.objects.filter(ship__game_id=self.id, x_coord=x, y_coord=y).first()
        self.guesses += 1
        self.save()
        if coord:
            coord.hit = True
            coord.save()
            return coord.ship
        return None

    def __str__(self):
        return 'Game #{0}({1})'.format(self.id, self.guesses)


class Ship(models.Model):
    SHIP_NAME_CHOICES = (
        ('CA', SHIPS['CA']['name']),
        ('BA', SHIPS['BA']['name']),
        ('CR', SHIPS['CR']['name']),
        ('SU', SHIPS['SU']['name']),
        ('DE', SHIPS['DE']['name']),
    )
    SHIP_LENGTH_CHOICES = (
        (SHIPS['CA']['length'], SHIPS['CA']['name']),
        (SHIPS['BA']['length'], SHIPS['BA']['name']),
        (SHIPS['CR']['length'], SHIPS['CR']['name']),
        (SHIPS['DE']['length'], SHIPS['DE']['name']),
        (SHIPS['SU']['length'], SHIPS['SU']['name']),
    )
    length = models.IntegerField('Length', choices=SHIP_LENGTH_CHOICES)
    name = models.CharField(
        'Name',
        max_length=2,
        choices=SHIP_NAME_CHOICES
    )
    game = models.ForeignKey(
        Game,
        related_name='ships',
        on_delete=models.CASCADE,
    )

    @property
    def sank(self):
        if self.coordinates.filter(hit=False):
            return False
        return True

    def __str__(self):
        return '{0}({1})'.format(self.name, self.length)


class Coordinate(models.Model):
    hit = models.BooleanField('Hit', default=False)
    x_coord = models.IntegerField('X coordinate')
    y_coord = models.IntegerField('Y coordinate')
    ship = models.ForeignKey(
        Ship,
        related_name='coordinates',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return '{} {}-{} ({})'.format(self.ship.name, self.x_coord, self.y_coord, self.hit and 'hit' or 'healthy')

