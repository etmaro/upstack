from django.conf.urls import url
from .views import NewGameAPI, StatusGameAPI, ShootAPI


urlpatterns = [
    url(r'^game/$', NewGameAPI.as_view(), name="new-game"),
    url(r'^game/(?P<game_id>\d+)/$', StatusGameAPI.as_view(), name="status-game"),
    url(r'^game/(?P<game_id>\d+)/shoot/$', ShootAPI.as_view(), name="shoot"),
]

