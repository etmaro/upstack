from rest_framework import serializers
from .models import Game


class GameSerializer(serializers.ModelSerializer):
    progress = serializers.SerializerMethodField()

    class Meta:
        model = Game
        fields = ('id', 'guesses', 'progress')

    def get_progress(self, obj):
        return obj.ships_remaining and "Remaining ships: {}".format(obj.ships_remaining) or 'No ships left. You won.'
