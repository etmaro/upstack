from django.contrib import admin
from .models import Game, Ship, Coordinate


admin.site.register(Game)
admin.site.register(Ship)
admin.site.register(Coordinate)
