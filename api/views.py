from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
from .models import Game
from .serializers import GameSerializer


class StatusGameAPI(APIView):
    """ /api/game/[<game_id>/]
    GET <game_id>: Status of the game
    """
    def get(self, request, game_id):
        game = get_object_or_404(Game, pk=game_id)
        return Response(GameSerializer(game).data)


class NewGameAPI(APIView):
    """ /api/game/
    POST: Creates new game
    """
    def post(self, request):
        game = Game.objects.create()
        game.add_ships()
        response_data = {}
        response_data['instructions'] = (
            'You started a new game. The board is 10x10. '
            'There are 5 ships on it with different length:'
            'Carrier (5), Battleship (4), Cruiser (3),'
            'Destroyer (2) and Submarine (1).'
        )
        response_data.update(GameSerializer(game).data)
        return Response(response_data)


class ShootAPI(APIView):
    """ /api/game/[<game_id>/]
    POST <game_id>: Recieves coordinates of shotting
    """
    def post(self, request, game_id):
        game = get_object_or_404(Game, pk=game_id)
        data = request.data
        response_data = {}
        try:
            x = int(data['x'])
            y = int(data['y'])
        except KeyError:
            raise ParseError('Coordinates not found or variables bad named')
        if x > 10 or x < 0 or y > 10 or y < 0:
            result = 'Out of bounds'
        else:
            ship = game.shoot(x, y)
            if ship:
                result = 'Hit ship'
                if ship.sank:
                    result = 'Sank {}'.format(ship.get_name_display())
            else:
                result = 'Hit water'
        response_data['coordinates_given'] = {'x': x, 'y': y}
        response_data['result'] = result
        response_data.update(GameSerializer(game).data)
        return Response(response_data)


