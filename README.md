# Battleship game

[![twitter](http://aboveandbelow.info/wp-content/uploads/2014/12/twitter-favicon-white-blue-website-20x20.jpg)](https://twitter.com/etmaroo)@etmaroo

This is a solo battleship game against the pc thru API.
# Tech used  
- [Python 3]
- [Django]
- [Django Rest Framework]
- [SQLite]
- [Behave rest]


# Installation   
Create virtualenv and activate
```sh
$ mkvirtualenv <project name>
$ workon <project name>
```

Install requirements 
```sh
$ pip install -r requirements.txt
```
Set database
```sh
$ ./manage.py makemigrations
$ ./manage.py migrate
```
Load test data
```sh
$ ./manage.py loaddata fixtures/0001_test.json
```
Run dev server
```sh
$ ./manage.py runserver
```
Run tests with behave
```sh
$ behave
```
# Endpoints
* `api/game/` : to start game
* `api/game/<game_id>/` : show status of the game
* `api/game/<game_id>/shoot/` : shoot to given coordinates in json        

# API Documentation

Powered by POSTMAN
<https://documenter.getpostman.com/view/1324062/RWM8SrKe>

   [behave rest]: <https://github.com/stanfy/behave-rest>
   [SQLite]: <https://www.sqlite.org/>
   [Python 3]: <https://www.python.org/download/releases/3.0/?>
   [Django]: <https://www.djangoproject.com/>
   [Django Rest Framework]: <http://www.django-rest-framework.org/>

2018
